Using sharness tests in Debian
------------------------------

A typical test script (e.g. "01.foo.t") could look like the following:


    #!/bin/sh

    test_description="some interesting attempts"

    . /usr/share/sharness/sharness.sh

    test_expect_success "some things work" "
      true
    "

    test_expect_failure "some things fail" "
      false
    "

    test_done


Please note the different location used above for sourcing the sharness library:
  * using an embedded copy: ". ./sharness.sh"
  * using the library provided by the Debian package: ". /usr/share/sharness/sharness.sh"


The test script can be executed via any TAP harness (e.g. "prove"):

    prove 01.foo.t


Using sharness for DEP8 tests (e.g. via autopkgtest)
----------------------------------------------------

Specification of DEP8 tests: https://dep-team.pages.debian.net/deps/dep8/
These tests are automatically executed by Debian's CI system: https://ci.debian.net/

The following steps are sufficient for creating a sharness-based test for a
package:
  * create a file debian/tests/control with the following content:
      Test-Command: prove debian/tests/*.t
      Depends: @,sharness
  * write one or more test scripts (see the above example) below debian/tests
    with the extension ".t"
  * verify your tests via autopkgtest (e.g. wrapped by sbuild) or watch the
    results produced by Debian's CI infrastructure
